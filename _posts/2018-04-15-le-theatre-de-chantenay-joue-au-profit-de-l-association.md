---
lang: fr
title: Le Théatre de Chantenay joue au profit de l'association.
date: 2017-12-23 11:38:45 +0000
layout: post
tagline: Une troupe de théâtre de Chantenay Villedieu présente une représentation
  au profit du projet
image: "/assets/img/theatre.jpg"
header:
  image: 
tags: []
keywords: ''
ref: theatre_2017
categories:
- articles
excerpt: ''
category: []
---
Le 29 octobre 2018, la troupe de théatre de Chantenay-Villedieu à joué sa pièce _Une pièce peut en cacher une autre_, au profil de l'association.

![Une pièce peut en cacher une autre](/assets/img/theatre.jpg)

La pièce a attiré 80 spectateurs, venant des communes aux alentours.