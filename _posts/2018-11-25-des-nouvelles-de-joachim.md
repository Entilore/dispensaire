---
lang: fr
category:
- articles
title: 'Des nouvelles de Joachim :'
layout: post
date: 2018-11-25 14:54:27 +0000
image: ''
tagline: 'Des nouvelles de Joachim avec qui nous communiquons essentiellement par
  mail : '
excerpt: 'Copie de différents mails reçus :'
tags: []
keywords: ''
ref: ''

---
28 janvier 2019 :

> "Merci pour votre soutien, j'ai promis dès réception de l'argent vous rendre compte de l'avancée et des difficultés même minimes que je vais rencontrer pour le projet, et je n'est pas failli, mais je garde la tête sur mes épaules car j'ai tout les papiers du terrain et j'ai aussi le soutien des propriétaires terriens qui sont des autochtones, , ils m'ont rassuré qu'ils se mettront au devant pour la confection de mes briques,
>
> Ce que j'ai pu faire déjà, c'est la construction du bassin pour le stockage de l'eau, le dépôt du sable pour les briques , le dépôt de latérite pour le comblement des trous et l’aplanissement du terrain pour la confection des briques.."

9 décembre 2018 :

> « Je compte prendre un congé administratif dans ce mois et je veux en profiter pour suivre la confection des briques pour le dispensaire".

12 novembre 2018 :

> « En ce qui me concerne je réitère ma gratitude pour vos efforts  en rapport avec mon projet ».

24 octobre 2018 :

> « Nous allons bien sauf qu'il fait chaud ce temps si car c'est une période de transition avec la période  froid, présentement en consultation ce sont des cas d'infection respiratoire aiguë et de paludisme ».

25 aout 2018 :

> « Ici nous rendons grâce à Dieu côté santé familiale, au service aussi tout va bien seulement c'est notre mauvaise période parce que nous avons beaucoup de malades présentement paludisme, maladie diarrhéiques....
>
> Le projet, j'attends toujours  je vous contacterai  quand je serai prêt parce que je ne veux pas  jouer avec votre soutien en perdant mon matériel de construction ».

7 juillet 2018 :

> « Je suis  toujours en négociation avec celui qui est sur mon terrain, raison pour laquelle je ne vous ai plus rien dis, des que situation décantée, je vous appellerai.
>
> Pour la confection des briques, je suis entrain de voir la faisabilité pour profiter de la saison pluvieuse.
>
> Mon salut à tous ».