---
lang: fr
category:
- articles
title: Photo du terrain
layout: post
date: 2018-07-07 00:00:00 +0000
image: "/assets/img/terrain-premiere.jpg"
tagline: Avec les explications de Joachim...
excerpt: Avec les explications de Joachim...
tags: []
keywords: terrain
ref: ''

---
![Première photo du terrain](/assets/img/terrain-premiere.jpg)

"J'ai essayé de montrer les limites du terrain en m'arrêtant sur les bornes avec un petit en habit rouge, moi j'y suis avec une chemise rouge"
