---
lang: fr
category:
- articles
title: Une nouvelle représentation théâtrale de la troupe de Chantenay
layout: post
date: 2018-11-19 19:29
image: "/assets/img/théatre 2018.JPG"
tagline: À qui perd dur !
excerpt: La pièce a été jouée en octobre 2018...
tags: []
keywords: théâtre
ref: théatre 2018.JPG

---
La pièce a été jouée en octobre 2018. Cela a permis de récupérer 300 euros au profit du dispensaire.

![](/assets/img/théatre 2018.JPG)