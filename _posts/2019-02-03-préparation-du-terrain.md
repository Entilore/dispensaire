---
lang: fr
category:
- articles
layout: post
title: Préparation du terrain
image: "/assets/img/IMG_20190114_105916.jpg"
tagline: Bassin pour le stockage de l'eau.
excerpt: 'Premières étapes : '
tags: []
keywords: ''
ref: ''
date: 2019-02-02 23:00:00 +0000

---
Premières étapes : la construction du bassin, le dépôt du changement de sable, le remblais en terre de quelques trous  sur le site pour aplanir le terrain et pouvoir confectionner les briques.