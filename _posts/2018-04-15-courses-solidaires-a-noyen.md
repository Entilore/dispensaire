---
lang: fr
title: Courses solidaires, à Noyen
date: 2018-03-22 01:00:00 +0000
layout: post
tagline: L'école Notre Dame du Sacré-Cœur organise une course solidaire au profit
  du dispensaire de Yagma
ref: course-solidaire-noyen
keywords: course-solidaire
image: "/assets/img/ecole_herbe.jpg"
header:
  image: ''
tags: []
excerpt: ''
categories: []
category: []

---
Le 24 mai dernier avait lieux une course solidaire, au profit du dispensaire de Yagma.

![La course solidaire](/assets/img/ecole_herbe.jpg)

Les élèves des écoles (La Suze, Roëzé, Fillé et Noyen) ont couru ensemble et assisté à une présentation de la vie des écoliers à Ouagadougou

![Les élèves assistent à la présentation](/assets/img/ecole_gymnase.jpg)

Cet évènement a permis de rapporter 977.85€ à l'association pour le projet.