---
layout: page
title: Plans
tagline: Les plans du futur dispensaire de Yagma
excerpt: Les plans du dispensaire de Yagma
permalink: "/plans.html"
header:
  image: "/assets/patterns/upfeathers.png"
ref: about
order: 3
---

{::options parse_block_html="true" /}

Voici les plans du dispensaire, tels qu'ils ont été conçus par Achil Séraphin PARE, architecte à Ouagadougou.

<div style="text-align: center">

  ![Plan de masse](/assets/img/plans/masse.svg)

  ![Vue en plan](/assets/img/plans/vue_en_plan.svg)

  ![Vue en coupe](/assets/img/plans/coupe.svg)

</div>