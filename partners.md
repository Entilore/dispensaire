---
layout: page
title: Partenaires
tagline: Les membres du projet
excerpt: Les membres du projet
permalink: "/partenaires.html"
header:
  image: "/assets/patterns/wov.png"
ref: contact
order: 4
---
{::options parse_block_html="true" /}

Diverses partenaires nous aident à la collecte des fonds nécessaires à la création du dispensaire de Yagma.

## L'association LOCAL PARTAGE

Elle a été  créé par Melle Clémentine, il y a très longtemps, elle récupérait de  vieux vêtements pour les donner aux plus pauvres.

Des gens donnent des  vêtements, des couvertures, des objets, de la vaisselle. Ce qui est de  bonne qualité est vendu 1, 2, 3... euros.  Ce qui est trop vieux est  donné à l'association "Emmaüs". Ce qui est usé, cassé est envoyé à la  déchetterie.

13 bénévoles travaillent à l’association "Local partage".

L'argent récoltée sert à :

* donner à L'Afrique - école - dispensaire
* à Emmaüs pour accueillir les SDF..
* aux restos du cœur
* jeunes agriculteurs du Burkina Faso.

\(texte écrit par les élèves de l'école de Noyen)

## Des écoles catholiques en Sarthe

![Notre Dame du Sacré-Cœur](/assets/img/ecoles.jpg)

* Notre Dame du Sacré-Cœur, à Noyen Sur Sarthe
* Sacré-Coeur de La Suze
* Saint Martin de Roëzé sur Sarthe
* Saint Charles de Fillé sur Sarthe

<figure style="text-align: center">
<img src="{{ "/assets/img/ecoles/nddsc.jpg" | prepend: site.baseurl }}" style="height: 130px; max-width: 30%">
<img src="{{ "/assets/img/ecoles/sc.jpg" | prepend: site.baseurl }}" style="height: 130px; max-width: 30%">
![](/assets/img/école Fillé.jpg)

... qui contribuent au financement par des actions solidaires (courses solidaires).

[En savoir plus... ]({{ site.baseurl }}{% post_url 2018-04-15-courses-solidaires-a-noyen %})

## Une troupe de théâtre de Chantenay Villedieu

![Notre Dame du Sacré-Cœur](/assets/img/theatre.jpg)

... qui présente une représentation au profit du projet.

[En savoir plus... ]({{ site.baseurl }}{% post_url 2018-04-15-le-theatre-de-chantenay-joue-au-profit-de-l-association %})

{% include go-to-home-page.html %}