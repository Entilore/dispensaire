---
layout: home
permalink: /
header:
  image: /assets/img/home-header.jpg
  title: Dispensaire Burkina
  tagline: Soutenez avec nous la création d’un dispensaire au Burkina Faso !
  excerpt: Soutenez avec nous la création d’un dispensaire au Burkina Faso !
repository:
  is_project_page: false
ref: home
---

<div style="text-align: center;">
 <h1>Soutenez avec nous la création d’un dispensaire au Burkina Faso !</h1>
</div>

<div id="slider">
  <figure style="text-align: center;">
    <img src="{{ "/assets/img/bebe.jpg" | prepend: site.baseurl }}" alt="Un bébé">
    <img src="{{ "/assets/img/joachim_bebe_regarde.jpg" | prepend: site.baseurl }}" alt="Joachim et un bébé">
  </figure>

  {%- include goal-bar.html -%}
    

  <figure style="text-align: center;">
    <img src="{{ "/assets/img/afrique.png" | prepend: site.baseurl }}" alt="Le Burkina Fasso dans l'Afrique">
    <img src="{{ "/assets/img/BF.jpg" | prepend: site.baseurl }}" alt="Le Burkina Fasso">
  </figure>

  <p> Il s'agit de construire un cabinet de soins infirmiers dans la banlieue de Ouagadougou, dépourvue de structures de ce genre. </p>
    

  <figure style="text-align: center;">
    <img src="{{ "/assets/img/paysage-afrique.jpg" | prepend: site.baseurl }}" alt="Paysage du Burkina">

  </figure>


</div>


## Sommaire 

{% assign pages = site.pages | sort: 'order' %}
{%- for page in pages -%}
{%- if page.order -%}
* [{{page.title}}]({{page.url | prepend: site.baseurl }})
{% endif %}
{%- endfor -%}

<h2>Actualités</h2>
<div>&nbsp;</div>
{%- include list-category-posts.html category="articles" -%}
---
