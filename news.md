---
layout: page
title: Actualités
permalink: /posts.html
header:
  image: /assets/img/home-header.jpg
tagline: > # this means to ignore newlines until "repository:"
  Les dernières nouveautés
excerpt: >
  Les dernières nouveautés
ref: posts
order: 5
---
{%- include list-category-posts.html lang=page.lang category="articles" show-pictures=true -%}

---
