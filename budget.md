---
layout: page
title: Budget
tagline: Financement et budget du projet de dispensaire à Yagma (Ouagadougou, Burkina
  Fasso)
excerpt: 'Financement et budget du projet de dispensaire à Yagma (Ouagadougou, Burkina
  Fasso) '
permalink: "/budget.html"
header:
  image: "/assets/patterns/upfeathers.png"
ref: bugdet
order: 2
image: []
tags: []
keywords: ''
lang: fr
date: ''
categories: []
category: []

---
## ![chaudron](/assets/img/argentxxx.gif)Financement - Budget :

Le projet comporte deux volets : l’achat du terrain et le financement du bâtiment.

### Le financement du terrain :

L’achat du terrain a été entièrement financé par un emprunt personnel de Monsieur Guissou.

_coût :_ **4 250 000 francs CFA** (soit **6480 euros**) Monsieur Guissou a emprunté une somme importante, réalisée en étudiant les montants à rembourser tout en continuant à assurer la subsistance de sa famille. Son engagement personnel conséquent garantit la nécessité pour lui de mener à son terme ce projet afin de pouvoir rembourser cet emprunt.

### Le financement du bâtiment :

L’association « [**Local Partage**]({{ site.baseurl}}{% link partners.md %}#lassociation-local-partage) «  contribue à la réalisation du projet par un soutien administratif (collecte de fonds) et par des aides financières.

La projet revient à un montant total de 6476430 francs CFA (soit  9875 euros) ce qui comprend la construction du bâtiment à l’exception des panneaux solaires, et du mur d’enceinte, lesquels pourraient être financés dans un deuxième temps.

Les ressources de l’association pour ce projet sont aujourd’hui de **{{site.collected}} euros** . Cette somme d’argent a été obtenue grâce à des subventions diverses obtenues par l'association Local Partage, (dont 1000 euros par le Crédit Agricole de Cérans Foulletourte),   à l’implication d’enfants d’écoles catholiques sarthoises  (près de 1000 euros aujourd’hui récoltés par des _actions solidaires_), ainsi que d’une troupe de théâtre sarthoise qui a effectué une _représentation_ au profit du projet.

Il reste aujourd’hui à collecter **{{ site.goal | minus: site.collected }} euros** pour la construction immédiate.

Nous faisons donc appel à votre générosité pour aider au financement de ce projet. Vous pouvez le faire en faisant un don :

* directement en ligne en passant par le site de financement participatif.
* en espèce ou chèque à l’ordre de Local Partage adressé à

  <address>
  Stéphanie et Franck Taugourdeau <br>
  La Goyonnière<br>
  72270 Malicorne<br>  
  </address>

  ou bien à

  <address>
  Claire et Olivier Hareau<br>
  16 rue de la libération<br>
  72430 Chantenay Villedieu  <br>
  </address>

{%- include goal-bar.html -%}

<p style="text-align: center">
<img src="{{'/assets/img/argentxxx.gif' | prepend: site.baseurl}}" alt="chaudron">
</p>