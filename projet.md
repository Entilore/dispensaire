---
layout: page
title: Projet
tagline: Quelques mots à propos de ce projet...
excerpt: Quelques mots à propos de ce projet...
permalink: "/projet.html"
header:
  image: "/assets/patterns/upfeathers.png"
ref: about
order: 1

---
Il s'agit de la construction d'un dispensaire à Ouagadougou. Voici un extrait du projet, rédigé par Joachim.

![La façade principale](/assets/img/facade.svg)

## Présentation générale :

Ce projet vise comme objectif la construction d’un cabinet de soins infirmiers dans le secteur de Yagma.

**Pourquoi ce lieu ?**

Yagma fait partie des zones péri urbaine au nord ouest de Ouagadougou avec une forte population vivant dans des habitations spontanées ( zones non loties ou non viabilisées) et où il y a peu d'offres de soins sanitaires.

**Pour quels patients ?**

Ce centre de santé sera ouvert à toute population sans distinction.

**Description du bâtiment :**

Il s’agit dans ce projet de la construction d’un bâtiment comportant une salle de consultation, une salle de soins et une salle de mise en observation.

Pour plus d'informations sur les plans du bâtiment, [veuillez accéder à l'onglet plan](/plans.html).

## Présentation du responsable :

{%- assign item = site.members | where: "author-id", "joachim" | first -%} {%- assign namePosition = "content" -%} {% assign loopindex = 1 %}

{% include display-member.html loopindex=page.loopindex item=page.item namePosition=page.namePosition %}

## Contexte et justification :

Le Burkina Faso s’est résolument engagé à vaincre la pauvreté en formulant et en mettant en œuvre son cadre stratégique de lutte contre la pauvreté depuis 2000. Pour cela des secteurs prioritaires ont été déterminés pour être intensifiés. Parmi ces secteurs, le secteur de la santé est un secteur prioritaire pour le développement de la nation. Ainsi la conjugaison de l’amélioration de l’offre de soins, de la qualité et de l’utilisation des services de santé a contribué à relever l’espérance de vie à la naissance. Les taux de mortalité infantile, infanto-juvénile et maternelle, bien qu’en baisse, demeurent encore élevés par rapport aux normes internationales. Les défis liés à l’inégale répartition des infrastructures sanitaires et le déficit de personnel, notamment en milieu rural doivent être relevés pour améliorer l’état de santé des populations. Cela exige l’accroissement des ressources (humaines, matérielles et financières) allouées au secteur de la santé.

<div class="row">
<div style="width: 60%; float: right; margin-left: 5%; margin-bottom: 10px;">
<img src="/assets/img/graphique mortalité infantile.jpg" alt="Mortalité infantile au Burkina Faso">
</div>
<div>
Par ailleurs, des mesures devront être prises pour corriger la mauvaise répartition des services de santé et leur faible utilisation. Les sous-secteurs privés sont des partenaires incontournables qui devront être mieux pris en compte dans les politiques de santé. La rationalisation de la gestion des structures publiques sanitaires et hospitalières, ciblée en particulier sur la maîtrise des coûts de prestations, l’amélioration de la fréquentation des formations sanitaires et le renforcement de la politique de rationalisation de la consommation des médicaments à tous les niveaux (tout en continuant à encourager l’industrie pharmaceutique locale), sont les principaux défis à relever pour les années à  venir. Le financement de la santé est indispensable pour créer les conditions favorables à la réforme du système national de santé, notamment celle des hôpitaux (modernisation, efficience dans la gestion, rendement).
</div>
</div>

C’est dans cette optique que le présent projet de création d’un cabinet de soins est formulé et soumis à votre appréciation.